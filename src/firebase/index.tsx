import firebase from 'firebase'

const firebaseConfig = {
    apiKey: "AIzaSyC-6uyo6w166qTG4hS561P9fT1kSfA-tZE",
    authDomain: "snake-801a0.firebaseapp.com",
    databaseURL: "https://snake-801a0-default-rtdb.firebaseio.com",
    projectId: "snake-801a0",
    storageBucket: "snake-801a0.appspot.com",
    messagingSenderId: "638897929437",
    appId: "1:638897929437:web:9e8b140086b343b0ef6f34"
};
  
firebase.initializeApp(firebaseConfig);
const database = firebase.database()
const db = database.ref()

export { database }
export default db;
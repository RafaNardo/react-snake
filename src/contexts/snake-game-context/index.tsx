import React, { useContext, useState } from 'react'
import { createContext } from 'react'
import { Snake } from '../../models'

interface ISnakeGameContextProps {
    snake: Snake,
    setSnake: React.Dispatch<React.SetStateAction<Snake>>,
    direction: string,
    setDirection: React.Dispatch<React.SetStateAction<string>>
}

const SnakeGameContext = createContext({} as ISnakeGameContextProps)

const SnakeGameProvider: React.FC = ({children}) => {

    const [snake, setSnake] = useState(new Snake())
    const [direction, setDirection] = useState('ArrowRight')

    return (
        <SnakeGameContext.Provider value={{snake, setSnake, direction, setDirection}}>
            {children}
        </SnakeGameContext.Provider>
    )
}

export function useSnakeGame() {
    return useContext(SnakeGameContext)
}

export default SnakeGameProvider
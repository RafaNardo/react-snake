import GlobalStyle from './styles/global-style';
import { Layout} from './components'
import { NewGamePage, PlayPage, RankingPage } from './pages'
import {
  BrowserRouter as Router,
  Switch,
  Route
} from 'react-router-dom';

const { Header, Body } = Layout

function App() {

  return (
    <>
      <Header />
      
      <Body>
        <Router>
          <GlobalStyle />
          <Switch>
            <Route path="/" exact>
              <NewGamePage />
            </Route>
            <Route path="/play" exact>
              <PlayPage />
            </Route>
            <Route path="/ranking" exact>
              <RankingPage />
            </Route>
          </Switch>
        </Router>
      </Body>

    </>
  );
}

export default App;

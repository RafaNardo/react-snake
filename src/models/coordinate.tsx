export default class Coordinate {
    constructor(public x: number = 0, public y: number = 0) { }

    public match = (coordinate: Coordinate) => coordinate.x === this.x && coordinate.y === this.y

    public static CreateRandom(min: number, max: number) {
        return new Coordinate(
            Math.floor(Math.random() * (max - min + 1)) + min,
            Math.floor(Math.random() * (max - min + 1)) + min
        )
    }
}



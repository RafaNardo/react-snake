import Coordinate from "./coordinate";

export default class Snake {
    
    constructor(
        public coordinates: Coordinate[] = [ new Coordinate(), new Coordinate(1, 0), new Coordinate(2, 0) ],
        public meat: Coordinate = new Coordinate(8, 9),
        public score: number = 0
    ) {
    }

    get head(): Coordinate {
        return this.coordinates[this.coordinates.length - 1];
    }

    match(coordinate: Coordinate): boolean {
        let foundCoordinate = this.coordinates.find(c => c.match(coordinate));

        return foundCoordinate !== undefined
    }

    private makeMeat = () => {
        let meatCoordinate = Coordinate.CreateRandom(1, 19)

        while(this.match(meatCoordinate))
            meatCoordinate = Coordinate.CreateRandom(1, 19)

        return meatCoordinate
    }

    public moveRight = () => {
        const head = this.head
        return this.move(new Coordinate(head.x + 1, head.y))
    }
    
    public moveLeft = () => {
        const head = this.head
        return this.move(new Coordinate(head.x - 1, head.y))
    }
    
    public moveTop = () => {
        const head = this.head
        return this.move(new Coordinate(head.x, head.y - 1))
    }
    
    public moveBottom = () => {
        const head = this.head
        return this.move(new Coordinate(head.x, head.y + 1))
    }
    
    private move = (newCoordinate: Coordinate) => {

        if (newCoordinate.x > 19
            || newCoordinate.y > 19
            || newCoordinate.x < 0
            || newCoordinate.y < 0
            || this.match(newCoordinate))
            return true

        if(this.meat.match(newCoordinate))
        {
            this.coordinates.push(newCoordinate)
            this.score += 10;
            this.meat = this.makeMeat()
        }
        else
        {
           this.coordinates.push(newCoordinate)
           this.coordinates.shift()
        }
        return false
    }
}
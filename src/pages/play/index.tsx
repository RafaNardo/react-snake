import { useLocation } from 'react-router-dom';
import { SnakeGame } from '../../components'
import { SnakeGameContext } from "../../contexts";


const Page =()=>{
    const query = new URLSearchParams(useLocation().search)

    return(
        <SnakeGameContext>
            <SnakeGame nickName={query.get("nickName") ?? ""} size={20} />
        </SnakeGameContext>
    )
}


export default Page
import { useState } from "react";
import styled from "styled-components";
import { Card, Input } from '../../components'
import { useHistory } from "react-router";


const Wrapper = styled.div`   
  display: flex;   
  flex-direction: column;
  justify-content:center;
  align-items: center;
  width: 400px;
  height: 400px;
`;

const Page =()=>{
    const [nickName, setNickName] = useState("")

    const history = useHistory()

    const onPlayClick = () => {
        const queryParameters = new URLSearchParams({ nickName });

        history.push(`/play?${queryParameters.toString()}`)
    }

    return(
        <Card>
            <Wrapper>
                <Input.Text placeholder="Nickname" value={nickName} onChange={e => setNickName(e.target.value)}></Input.Text>                
                <Input.Button value="Play" onClick={onPlayClick} disabled={!nickName} />
            </Wrapper> 
        </Card>
    );    
}


export default Page
import { Card, Input } from '../../components'
import styled from "styled-components";
import { useEffect, useState } from "react";
import db from '../../firebase'
import { useHistory } from 'react-router';
import { Score } from '../../models'
import { useLocation } from 'react-router-dom';

const Wrapper = styled.div`   
  display: flex;   
  flex-direction: column;
  justify-content:center;
  align-items: center;
  width: 400px;
  height: 400px;
`;

interface IScorePanelProps {
    scores: Score[]
}

const ScorePanel: React.FC<IScorePanelProps> = ( {scores} ) => {

    

    const top10  = scores.sort((a, b) => b.score - a.score).slice(0, 10)

    return (
        <>
            { top10.map((s, i) => <div key={i}>{s.nome} - {s.score}</div>) }
        </>
    )
}

const Page = () => {
    const query = new URLSearchParams(useLocation().search)
    const nickName = query.get("nickName") as string;
    const score = query.get("score");    
    
    const history = useHistory()
    const onRestartClick = () => {
        const queryParameters = new URLSearchParams({ nickName: nickName });

        history.push(`/play?${queryParameters.toString()}`)
    }

    const onNewGameClick = () => {
        history.push(`/`)
    }

    
    const [scores, setScores] = useState([] as Score[])

    const fetchScores  = async ()=> {
        const rankings = db.child("rankings");
        
        rankings.on('value', (snapshot) => {
            if (snapshot.exists()) {


                const teste = [] as Score[]
                const horse = snapshot.val();

                Object.keys(horse).map(function(key, index) {
                    teste.push(new Score(horse[key].nome, horse[key].score))
                });

                setScores(teste)
            }
          });
    }

    useEffect(() => { fetchScores() }, [])

    return (
        <Card>
            <Wrapper>
                <h2>Your score: {score}</h2>
                <br />

                <ScorePanel scores={scores} />
                
                <br />
                <Input.Button value="Restart" onClick={onRestartClick} />
                <br />
                <Input.Button value="New Game" onClick={onNewGameClick} />
            </Wrapper>
        </Card>
    );
}


export default Page
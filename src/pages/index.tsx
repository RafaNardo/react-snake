import NewGamePage from './new-game'
import PlayPage from './play'
import RankingPage from './ranking'


export {
    NewGamePage,
    PlayPage,
    RankingPage
}
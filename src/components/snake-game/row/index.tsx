import Cell from '../cell'
import {Coordinate} from '../../../models'

interface IGameRowProps {
    index: number,
    cells: number[]
}

const Row: React.FC<IGameRowProps> = (props) => {

  return (
    <div key={props.index}>
      {
        props.cells.map((value) => {

          const c = new Coordinate(props.index, value)

          return <Cell key={value} coordinate={c}></Cell>
        })
      }
    </div>
  )
}

export default Row
import styled from "styled-components"
import { MatrizFactory } from "../../factories"
import Row from "./row"
import React, { useRef } from "react"
import { useSnakeGame } from "../../contexts/snake-game-context"
import { Snake } from "../../models"
import { useEffect } from "react"
import { useState } from "react"
import { useHistory } from "react-router"
import { database } from "../../firebase"

interface IGameProps {
  size: number,
  nickName: string
}

const Wrapper = styled.div`
  display: flex;
  padding: 1px;
  flex-wrap: unset;
  border: 5px solid black;
  border-radius: 10px; 
`

const WrapperGeral = styled.div`
  display: flex;
  flex-direction: column;
`

const Game: React.FC<IGameProps> = (props) => {
  const history = useHistory()
  const inputEl = useRef<HTMLInputElement>(null);

  const [direction, setDirection] = useState('ArrowRight')

  const matriz = MatrizFactory.Create(props.size)

  const context = useSnakeGame()

  const snake = context.snake

  useEffect(() => {

    const keyDownEvent = (event: KeyboardEvent) => {

      if (direction === "ArrowRight" && event.key === "ArrowLeft") return
      else if (direction === "ArrowLeft" && event.key === "ArrowRight") return
      else if (direction === "ArrowDown" && event.key === "ArrowUp") return
      else if (direction === "ArrowUp" && event.key === "ArrowDown") return

      setDirection(event.key)
    }

    window.document.addEventListener("keydown", keyDownEvent);

    return () => {
      window.document.removeEventListener("keydown", keyDownEvent);
    }
  }, [direction])

  useEffect(() => {
    setInterval(() => { inputEl.current?.click() }, 110)
  }, [])


  const onClick = () => { move() }

  const move = () => {
    let gameOver = false;

    switch (direction) {
      case 'ArrowLeft':
        gameOver = snake.moveLeft()
        break;
      case 'ArrowRight':
        gameOver = snake.moveRight()
        break;
      case 'ArrowDown':
        gameOver = snake.moveBottom()
        break;
      case 'ArrowUp':
        gameOver = snake.moveTop()
        break;
      default:
        break;
    }

    if (gameOver)
    {
      const teste = {
        nickName: props.nickName, 
        score: snake.score.toString()
      }

      const queryParameters = new URLSearchParams({ ...teste })
      
      database.ref("rankings").push({ 
        nome: props.nickName,
        score: snake.score.toString()
       });


      history.push(`/ranking?${queryParameters.toString()}`)
    }

    context.setSnake(new Snake(snake.coordinates, snake.meat, snake.score))
  }

  return (
    <WrapperGeral>
      <div style={{ display: "flex", justifyContent: "space-between" }}>
        <span style={{ fontWeight: "bold", fontSize: "1.4em" }}>Nick: {props.nickName}</span>
        <span style={{ fontWeight: "bold", fontSize: "1.4em", paddingRight: "15px" }}>Score: {snake.score}</span>
      </div>
      <Wrapper>
        {matriz.map((cells, index) => <Row key={index} {...{ index, cells }} ></Row>)}

        <input type="hidden" ref={inputEl} onClick={onClick}></input>
      </Wrapper>
    </WrapperGeral>
  )
}

export default Game
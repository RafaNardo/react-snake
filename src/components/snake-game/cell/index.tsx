import styled, { css } from "styled-components";
import {Coordinate} from '../../../models'
import {useSnakeGame} from '../../../contexts/snake-game-context'

interface IGameCellProps {
    type?: 'meat'|'head'|'body'
    coordinate: Coordinate
}

const Wrapper = styled.div<IGameCellProps>`
    width: 20px;
    height: 20px;
    border-radius: 5px;

    ${props => props.type && css`border: 2px solid black;`}

    ${props => props.type === 'meat' && css`background-color: red;`}
    ${props => props.type === 'head' && css`background-color: black;`}
    ${props => props.type === 'body' && css`background-color: gray;`}
  `

const GameCell: React.FC<IGameCellProps> = (props) => {
  const {snake} = useSnakeGame()
  
  if (snake.meat.match(props.coordinate))
    return <Wrapper {...props} type={"meat"}/>
  else if (snake.head.match(props.coordinate))
    return <Wrapper {...props} type={"head"}/>
  else if (snake.match(props.coordinate))
    return <Wrapper {...props} type={"body"}/>
  else
    return <Wrapper {...props} />
}

export default GameCell
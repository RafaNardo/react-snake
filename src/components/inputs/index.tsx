import Text from './text'
import Button from './button'

const Inputs = {
    Text,
    Button
}

export default Inputs
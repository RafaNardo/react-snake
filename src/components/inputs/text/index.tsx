import styled from "styled-components";

const TextInput = styled.input.attrs({ type: "text" })`
    background-color: transparent;
    font-weight: bold;
    font-size: 1.2em;

    outline: 0;
    border-width: 0 0 2px;
    border-color: black;

    margin-bottom: 10px;
    width: 200px;

    transition: all .2s ease-out;
    /* transition: box-shadow 2.5s ease, border-color 2.5s ease; */

    :focus {
        border: 3px solid;
        border-radius: 5px;
    }

    ::placeholder {
        color: black;
    }
`


export default TextInput
import styled from "styled-components";

const ButtonInput = styled.input.attrs({ type: "button" })`
    font-weight: bold;
    font-size: 1.2em;
    border-color: 5px solid black;
    width: 200px;
    border: 2px solid black;
    border-radius: 5px;
    color: black;
    background-color: transparent;

    :hover {
        border: 3px solid black;
    }

    :disabled {
        border: 3px solid gray;
    }

    :active {
        border-bottom: 4px solid black;
        border-right: 4px solid black;
        padding-top: 2px;
        padding-left: 2px;
    }
    
`


export default ButtonInput
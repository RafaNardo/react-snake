import styled from "styled-components";

const Wrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-around;
  height: 100px;
  background-color: #6bc14d;
`
const TextWrapper = styled.span`
    color: black;
    font-weight: bold;
    height: 32px;
    line-height: 32px;
    margin: 0 4px 0 0;
    padding: 0;
    white-space: nowrap;
    font-size: 1.7rem;
`

const Header = () => {
    return (
        <Wrapper>
            <TextWrapper>
                Snake dos gurí
            </TextWrapper>
        </Wrapper>
    )
}

export default Header
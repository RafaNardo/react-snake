import Header from './header'
import Body from './body'

const Layout = {
    Header,
    Body
}

export default Layout
import styled from "styled-components";

const Body = styled.main`
  display: flex;
  flex-grow: 1;
  align-items: center;
  justify-content: space-around;
  height: 100px;
  background-color: #86e267;
`

export default Body
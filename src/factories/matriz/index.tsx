class MatrizFactory {
    private static enumerarArray = (start: number, end: number) => { //fillRange
        return Array(end - start + 1).fill(0).map((item, index) => start + index)
    };

    static Create(dimensao: number) {
        const dimensaoArray = dimensao - 1

        const linhas = this.enumerarArray(0, dimensaoArray);

        return linhas.map(i => this.enumerarArray(0, dimensaoArray))
    }
}

export default MatrizFactory